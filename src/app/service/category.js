import mongoose from "mongoose";
import Category from "../model/category";

const service = {
  addCategory: model => {
    return new Promise((resolve, reject) => {
      Category.findOneAndUpdate(
        { _id: model._id },
        { $set: { title: model.title, parent: model.parent } },
        { upsert: true, new: true }
      )
        .then(res => resolve(res))
        .catch(error => reject(error));
    });
  },
  getCategories: () => {
    return Category.find({});
  },

  addRecursive: async category => {
    const model = new Category({
      _id: category._id ? category._id : new mongoose.Types.ObjectId(),
      title: category.title,
      parent: category.parent
    });
    const parent = await service.addCategory(model);

    if (category.children) {
      Array.from(category.children).forEach(item => {
        item.parent = parent._id; // eslint-disable-line
        service.addRecursive(item);
      });
    }
  },
  removeCategory: category => {
    return new Promise((resolve, reject) => {
      let filter = null;
      filter = category._id ? { _id: category._id } : { title: category.title };
      return Category.findOneAndRemove(filter)
        .then(response => resolve(response))
        .catch(error => reject(error));
    });
  },
  removeRecursive: async category => {
    if (category.children.length > 0) {
      Array.from(category.children).forEach(async item => {
        await service.removeRecursive(item);
      });
    }
    await service.removeCategory(category);
  }
};

export default service;
