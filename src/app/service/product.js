import Product from "../model/product";

const Service = {
  addProduct: data => {
    return Product.create(data);
  }
};

export default Service;
