import express from "express";
import Service from "../service/category";

const router = express.Router();

router.post("/", (req, res) => {
  try {
    Array.from(req.body).forEach(async cat => {
      cat.parent = null;
      await Service.addRecursive(cat);
    });
    res.status(201).send({ message: "Categories added" });
  } catch (err) {
    res.status(400).send({ message: "Something went wrong" });
  }
});

router.get("/", (req, res) => {
  Service.getCategories()
    .then(response => {
      res.send(response);
    })
    .catch(error => {
      res.status(400).send({ message: "Something went wrong" });
    });
});
router.delete("/", async (req, res) => {
  const category = req.body;

  await Service.removeRecursive(category)
    .then(() => {
      res.send({ message: "Category remove" });
    })
    .catch(err => {
      res.status(400).send({ message: "Something went wrong" });
    });
});
export default router;
