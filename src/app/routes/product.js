import express from "express";
import mongoose from "mongoose";
import Product from "../model/product";
import Service from "../service/product";
import multer from "multer";

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => cb(null, "src/assets/images/product"),
  filename: (req, file, cb) => cb(null, file.originalname)
});
const multierMiddleWare = multer({ storage });

router.post("/", multierMiddleWare.single("productImage"), (req, res) => {
  const { body, file } = req;
  const product = new Product({
    _id: new mongoose.Types.ObjectId(),
    name: body.name,
    amount: body.amount,
    quantity: body.quantity,
    productImage: file.path,
    categoryId: body.categoryId
  });

  Service.addProduct(product)
    .then(response => {
      res.status(200).send(response);
    })
    .catch(error => {
      res.status(400).send(error);
    });
});
export default router;
