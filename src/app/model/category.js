import mongoose from "mongoose";

const schema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  title: { type: String, required: true },
  parent: { type: mongoose.Schema.Types.ObjectId, ref: "category" }
});

export default mongoose.model("category", schema);
