import mongoose, { mongo } from "mongoose";

const schema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: { type: String, required: true },
  amount: { type: Number, required: true },
  quantity: { type: Number, required: true },
  productImage: { type: String, required: true },
  categoryId: { type: mongoose.Schema.Types.ObjectId, ref: "category" }
});

export default mongoose.model("product", schema);
