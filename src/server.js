import express from "express";
import bodyParser from "body-parser";

import mongoose from "mongoose";
import categoryRoutes from "./app/routes/category";
import productRoutes from "./app/routes/product";

mongoose
  .connect(
    "mongodb://admin:admin@shopping-app-shard-00-00-tr6fh.mongodb.net:27017,shopping-app-shard-00-01-tr6fh.mongodb.net:27017,shopping-app-shard-00-02-tr6fh.mongodb.net:27017/test?ssl=true&replicaSet=shopping-app-shard-0&authSource=admin&retryWrites=true",
    { useNewUrlParser: true, useCreateIndex: true }
  )
  .then(() => console.log("Mongo Connected"))
  .catch(() => console.log("Mongo Connection Error"));
mongoose.set("useFindAndModify", false);
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const PORT = 5000;

app.use((req, res, next) => {
  res.header("Access-Control-Allow-origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});
app.use("/api/category", categoryRoutes);
app.use("/api/product", productRoutes);

app.listen(
  PORT,
  () => console.log(`Listening on port => ${PORT}`) // eslint-disable-line
);
